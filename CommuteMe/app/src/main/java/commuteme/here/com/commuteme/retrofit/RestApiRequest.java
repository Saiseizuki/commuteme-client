package commuteme.here.com.commuteme.retrofit;

import android.util.Log;
import android.widget.Toast;

import commuteme.here.com.commuteme.CommuteMeApplication;
import commuteme.here.com.commuteme.activeandroid.DaoActiveAndroid;
import commuteme.here.com.commuteme.activeandroid.ParseActiveAndroid;
import commuteme.here.com.commuteme.event.FailedRequestJourney;
import commuteme.here.com.commuteme.event.FailedRequestPoints;
import commuteme.here.com.commuteme.event.SuccessRequestJourney;
import commuteme.here.com.commuteme.event.SuccessRequestPoints;
import commuteme.here.com.commuteme.model.ErrorResponseModel;
import commuteme.here.com.commuteme.model.JourneyResponseModel;
import commuteme.here.com.commuteme.model.PointsResponseModel;
import de.greenrobot.event.EventBus;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by jadeantolingaa on 12/6/14.
 */
public class RestApiRequest {
  public static String TAG = RestApiRequest.class.getSimpleName();
  private ParseActiveAndroid mParseActiveAndroid;
  private DaoActiveAndroid mDaoActiveAndroid;

  public RestApiRequest() {
    this.mParseActiveAndroid = new ParseActiveAndroid();
    this.mDaoActiveAndroid = new DaoActiveAndroid();
  }

  public void requestGetJourney(String startLat, String startLng, String endLat, String endLng) {
    RestAdapterHolder.getJourneyApi().getJourney(startLat, startLng,
            endLat, endLng, new Callback<JourneyResponseModel>() {

              @Override
              public void success(JourneyResponseModel journeyResponseModel, Response response) {
                Log.d(TAG, "@requestGetJourney success!");
                mParseActiveAndroid.parseJourneyToActiveAndroid(journeyResponseModel);
                EventBus.getDefault().post(new SuccessRequestJourney(journeyResponseModel));
              }

              @Override
              public void failure(RetrofitError retrofitError) {
                Log.d(TAG, "@requestGetJourney failed!");
                EventBus.getDefault().post(new FailedRequestJourney(new ErrorResponseModel
                        (retrofitError
                        .getMessage())));
              }
            }
    );
  }

  public void requestGetPoints() {
    RestAdapterHolder.getJourneyApi().getPoints(new Callback<PointsResponseModel>() {
      @Override
      public void success(PointsResponseModel pointsResponseModel, Response response) {
        Log.d(TAG, "@requestGetPoints success!");
        mParseActiveAndroid.parsePointsToActiveAndroid(pointsResponseModel);
        EventBus.getDefault().post(new SuccessRequestPoints(pointsResponseModel));
      }

      @Override
      public void failure(RetrofitError retrofitError) {
        Log.d(TAG, "@requestGetPoints failed!");
        Toast.makeText(CommuteMeApplication.getContext(), "Failed" + retrofitError.getMessage(), Toast.LENGTH_LONG ).show();
        EventBus.getDefault().post(new FailedRequestPoints(new ErrorResponseModel(retrofitError
                .getMessage())));
      }
    });
  }
}
