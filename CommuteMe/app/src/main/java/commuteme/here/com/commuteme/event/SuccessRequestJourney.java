package commuteme.here.com.commuteme.event;

import commuteme.here.com.commuteme.api.JourneyBundleRequest;
import commuteme.here.com.commuteme.model.JourneyResponseModel;

/**
 * Created by jadeantolingaa on 12/6/14.
 */
public class SuccessRequestJourney {
  public JourneyResponseModel mJourneyResponseModel;

  public SuccessRequestJourney(JourneyResponseModel journeyResponseModel) {
    this.mJourneyResponseModel = journeyResponseModel;
  }
}
