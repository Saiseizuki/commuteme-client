package commuteme.here.com.commuteme.api;

import commuteme.here.com.commuteme.model.JourneyResponseModel;
import commuteme.here.com.commuteme.model.PointsResponseModel;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by jadeantolingaa on 12/6/14.
 */
public interface CommuteApi {
  public static String TAG = CommuteApi.class.getSimpleName();

  @GET("/v1/journey")
  public void getJourney(@Query("startLat") String startLat, @Query("startLong") String
          startLong, @Query("endLat") String endLat, @Query("endLong") String endLong,
                         Callback<JourneyResponseModel> journeyCallback);

  @GET("/v1/points")
  public void getPoints(Callback<PointsResponseModel> pointsCallback);
}