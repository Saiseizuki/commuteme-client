package commuteme.here.com.commuteme;

import android.content.Context;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.app.Application;

/**
 * Created by jadeantolingaa on 12/6/14.
 */
public class CommuteMeApplication extends com.activeandroid.app.Application {
  private static Context mContext;

  @Override
  public void onCreate() {
    super.onCreate();
    mContext = getApplicationContext();
    ActiveAndroid.initialize(this);
  }

  public static Context getContext() {
    return mContext;
  }
}
