package commuteme.here.com.commuteme.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by jadeantolingaa on 12/6/14.
 */
@Table(name="journey_table")
public class JourneyModel extends Model{

  @Column(name = "journey_id")
  @SerializedName("journeyId")
  public String mJourneyId;

  @Column(name = "cost")
  @SerializedName("cost")
  public String mCost;

  @Column(name = "speed")
  @SerializedName("speed")
  public String mSpeed;

  @Column(name = "safety")
  @SerializedName("safety")
  public String mSafety;

  @Column(name = "eta")
  @SerializedName("eta")
  public String mEta;

  @Column( name = "estimatedCost")
  @SerializedName("estimatedCost")
  public String mEstimatedCost;

  public List<RoutesModel> routes;
}
