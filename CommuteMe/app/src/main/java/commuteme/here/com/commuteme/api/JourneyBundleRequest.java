package commuteme.here.com.commuteme.api;

/**
 * Created by jadeantolingaa on 12/6/14.
 */
public class JourneyBundleRequest {
  public final int mStartLat;
  public final int mStartLng;
  public final int mEndLat;
  public final int mEndLng;

  public JourneyBundleRequest(int startLat, int startLng, int endLat, int endLng) {
    this.mStartLat = startLat;
    this.mStartLng = startLng;
    this.mEndLat = endLat;
    this.mEndLng = endLng;
  }
}
