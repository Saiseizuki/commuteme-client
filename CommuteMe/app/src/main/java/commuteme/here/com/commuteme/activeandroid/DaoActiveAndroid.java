package commuteme.here.com.commuteme.activeandroid;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

import java.util.List;

import commuteme.here.com.commuteme.model.JourneyModel;
import commuteme.here.com.commuteme.model.PointsModel;
import commuteme.here.com.commuteme.model.RoutesModel;

/**
 * Created by jadeantolingaa on 12/6/14.
 */
public class DaoActiveAndroid {
  public static String TAG = DaoActiveAndroid.class.getSimpleName();

  //journey

  public List<JourneyModel> getJourney(String id) {
    return new Select()
            .from(JourneyModel.class)
            .where("journeyId = ?", id)
            .execute();
  }

  public List<JourneyModel> getJourneyArrangeBySpeed() {
    return new Select()
            .from(JourneyModel.class)
            .orderBy("speed DESC")
            .execute();
  }

  public List<JourneyModel> getJourneyArrangeByCost() {
    return new Select()
            .from(JourneyModel.class)
            .orderBy("cost DESC")
            .execute();
  }

  public List<JourneyModel> getJourneyArrangeBySafety() {
    return new Select()
            .from(JourneyModel.class)
            .orderBy("safety DESC")
            .execute();
  }

  public List<JourneyModel> getAllJourney() {
    return new Select()
            .from(JourneyModel.class)
            .execute();
  }

  public List<RoutesModel> getAllRoutesInJourney(JourneyModel journeyModel) {
    return new Select()
            .from(RoutesModel.class)
            .where("Journey = ?", journeyModel.getId())
            .execute();
  }

  //points

  public List<PointsModel> getAllPoints() {
    return new Select()
            .from(PointsModel.class)
            .execute();
  }

  public void deleteDatabase() {
    new Delete().from(RoutesModel.class).execute();
    new Delete().from(JourneyModel.class).execute();
  }
}
