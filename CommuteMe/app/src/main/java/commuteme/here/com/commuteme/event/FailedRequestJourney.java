package commuteme.here.com.commuteme.event;

import commuteme.here.com.commuteme.model.ErrorResponseModel;

/**
 * Created by jadeantolingaa on 12/6/14.
 */
public class FailedRequestJourney {
  public ErrorResponseModel mErrorResponseModel;

  public FailedRequestJourney(ErrorResponseModel errorResponseModel) {
    this.mErrorResponseModel = errorResponseModel;
  }
}
