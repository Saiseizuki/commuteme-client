package commuteme.here.com.commuteme.event;

import commuteme.here.com.commuteme.model.PointsResponseModel;

/**
 * Created by jadeantolingaa on 12/6/14.
 */
public class SuccessRequestPoints {
  public PointsResponseModel mPointsResponseModel;

  public SuccessRequestPoints(PointsResponseModel pointsResponseModel) {
    this.mPointsResponseModel = pointsResponseModel;
  }
}
