package commuteme.here.com.commuteme.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.SerializedName;

/**
 * Created by jadeantolingaa on 12/6/14.
 */
@Table(name = "route_table")
public class RoutesModel extends Model{

  @Column(name = "startLat")
  @SerializedName("startLat")
  public String mStartLat;

  @Column(name = "startLong")
  @SerializedName("startLong")
  public String mStartLng;

  @Column(name = "endLat")
  @SerializedName("endLat")
  public String mEndLat;

  @Column(name = "endLong")
  @SerializedName("endLong")
  public String mEndLng;

  @Column(name = "mode")
  @SerializedName("mode")
  public String mMode;

  @Column(name = "Journey")
  public JourneyModel journey;

}
