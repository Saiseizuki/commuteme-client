package commuteme.here.com.commuteme.retrofit;

import commuteme.here.com.commuteme.api.CommuteApi;
import commuteme.here.com.commuteme.utility.Constants;
import retrofit.RestAdapter;

/**
 * Created by jadeantolingaa on 12/6/14.
 */
public class RestAdapterHolder {
  public static String TAG = RestAdapterHolder.class.getSimpleName();
  private static CommuteApi mCommuteApi;

  public static CommuteApi getJourneyApi() {
    if (mCommuteApi == null) {
      RestAdapter restAdapter = new RestAdapter.Builder()
              .setEndpoint(Constants.JOURNEY_URL)
              .setLogLevel(RestAdapter.LogLevel.FULL)
              .build();

      mCommuteApi = restAdapter.create(CommuteApi.class);
    }
    return mCommuteApi;
  }
}
