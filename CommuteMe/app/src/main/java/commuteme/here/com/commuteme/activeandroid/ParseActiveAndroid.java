package commuteme.here.com.commuteme.activeandroid;

import com.activeandroid.ActiveAndroid;

import commuteme.here.com.commuteme.model.JourneyModel;
import commuteme.here.com.commuteme.model.JourneyResponseModel;
import commuteme.here.com.commuteme.model.PointsModel;
import commuteme.here.com.commuteme.model.PointsResponseModel;
import commuteme.here.com.commuteme.model.RoutesModel;

/**
 * Created by jadeantolingaa on 12/6/14.
 */
public class ParseActiveAndroid {
  public static String TAG = ParseActiveAndroid.class.getSimpleName();

  public void parseJourneyToActiveAndroid(JourneyResponseModel journeyResponseModel) {
    ActiveAndroid.beginTransaction();
    try {
      for (int position = 0; position < journeyResponseModel.journeys.size(); position++) {
        JourneyModel mJourneyModel = new JourneyModel();
        mJourneyModel.mJourneyId = journeyResponseModel.journeys.get(position).mJourneyId;
        mJourneyModel.mCost = journeyResponseModel.journeys.get(position).mCost;
        mJourneyModel.mSpeed = journeyResponseModel.journeys.get(position).mSpeed;
        mJourneyModel.mSafety = journeyResponseModel.journeys.get(position).mSafety;
        mJourneyModel.mEta = journeyResponseModel.journeys.get(position).mEta;
        mJourneyModel.mEstimatedCost = journeyResponseModel.journeys.get(position).mEstimatedCost;
        mJourneyModel.save();

          for(int routes = 0; routes < journeyResponseModel.journeys.get(position).routes.size(); routes++) {
            RoutesModel mRoutesModel = new RoutesModel();
            mRoutesModel.mStartLat = journeyResponseModel.journeys.get(position).routes.get(routes).mStartLat;
            mRoutesModel.mStartLng = journeyResponseModel.journeys.get(position).routes.get(routes).mStartLng;
            mRoutesModel.mEndLat = journeyResponseModel.journeys.get(position).routes.get(routes).mEndLat;
            mRoutesModel.mEndLng = journeyResponseModel.journeys.get(position).routes.get(routes).mEndLng;
            mRoutesModel.journey = journeyResponseModel.journeys.get(position);
          }
      }
      ActiveAndroid.setTransactionSuccessful();
    } finally {
      ActiveAndroid.endTransaction();
    }
  }

  public void parsePointsToActiveAndroid(PointsResponseModel pointsResponseModel) {
    ActiveAndroid.beginTransaction();
    try {
      for (int position = 0; position < pointsResponseModel.points.size(); position++) {
        PointsModel mPointsModel = new PointsModel();
        mPointsModel.mStartLat = pointsResponseModel.points.get(position).mStartLat;
        mPointsModel.mStartLong = pointsResponseModel.points.get(position).mStartLong;
        mPointsModel.mEndLat = pointsResponseModel.points.get(position).mEndLat;
        mPointsModel.mEndLong = pointsResponseModel.points.get(position).mEndLong;
        mPointsModel.save();
      }
      ActiveAndroid.setTransactionSuccessful();
    } finally {
      ActiveAndroid.endTransaction();
    }
  }
}
