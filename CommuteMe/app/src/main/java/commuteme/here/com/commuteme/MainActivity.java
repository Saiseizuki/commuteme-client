package commuteme.here.com.commuteme;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.here.android.mpa.common.GeoBoundingBox;
import com.here.android.mpa.common.GeoCoordinate;
import com.here.android.mpa.common.Image;
import com.here.android.mpa.common.OnEngineInitListener;
import com.here.android.mpa.common.ViewObject;
import com.here.android.mpa.mapping.Map;
import com.here.android.mpa.mapping.MapFragment;
import com.here.android.mpa.mapping.MapGesture;
import com.here.android.mpa.mapping.MapMarker;
import com.here.android.mpa.mapping.MapObject;
import com.here.android.mpa.mapping.MapRoute;
import com.here.android.mpa.routing.RouteManager;
import com.here.android.mpa.routing.RouteOptions;
import com.here.android.mpa.routing.RoutePlan;
import com.here.android.mpa.routing.RouteResult;

import java.util.ArrayList;
import java.util.List;

import commuteme.here.com.commuteme.model.JourneyModel;
import commuteme.here.com.commuteme.model.PointsModel;
import commuteme.here.com.commuteme.model.RoutesModel;
import commuteme.here.com.commuteme.activeandroid.DaoActiveAndroid;
import commuteme.here.com.commuteme.activeandroid.ParseActiveAndroid;
import commuteme.here.com.commuteme.event.SuccessRequestJourney;
import commuteme.here.com.commuteme.event.SuccessRequestPoints;
import commuteme.here.com.commuteme.retrofit.RestApiRequest;
import de.greenrobot.event.EventBus;


public class MainActivity extends Activity {

    private GeoCoordinate startPoint;
    private GeoCoordinate endPoint;

    private MapFragment mapFragment = null;
    private Map map = null;
    private RadioGroup footerRadioGroup;
    private ImageButton searchImageButton;

    private List<MapRoute> mapRouteList = null;
    private List<RoutePlan> routePlanList = null;
    private List<String> routeSpielsList = null;

    private List<MapObject> startEndPointsMapObjects = null;

    private List<RoutesModel> routesModelList = null;

    private int[] colorList;


    private int currRouteCalculated = 0;
    private int colorIndex = 0;

    private RestApiRequest mRestApiRequest;
    private DaoActiveAndroid mDaoActiveAndroid;
    private ParseActiveAndroid mParseActiveAndroid;

    private RouteManager.Listener routeManagerListener =
            new RouteManager.Listener() {

                public void onCalculateRouteFinished(RouteManager.Error errorCode,
                                                     List<RouteResult> result) {
                    if (errorCode == RouteManager.Error.NONE &&
                            result.get(0).getRoute() != null) {
                        // create a map route object and place it on the map
                        MapRoute mapRoute = new MapRoute(result.get(0).getRoute());

                        Image image = new Image();
                        image.setBitmap(BitmapFactory.decodeResource(getResources(),
                                R.drawable.map_marker));
                        MapMarker mapMarker = new MapMarker(mapRoute.getRoute().getStart(), image);
                        mapMarker.setTitle("Route details:\n");
                        mapMarker.setDescription("PUV type: " + routesModelList.get
                                (currRouteCalculated).mMode);
                        map.addMapObject(mapMarker);

                        mapRoute.setColor(colorList[colorIndex]);

                        RouteManager routeManager = RouteManager.getInstance();
                        if (currRouteCalculated < colorList.length) {
                            colorIndex++;
                        } else {
                            colorIndex = 0;
                        }
                        currRouteCalculated++;
                        if (currRouteCalculated < routePlanList.size()) {
                            routeManager.calculateRoute(routePlanList.get(currRouteCalculated),
                                    routeManagerListener);
                        } else {
                            colorIndex = 0;
                            double maxLatitude = 0; //max x
                            double minLatitude = 0; //min x
                            double maxLongitude = 0; //max y
                            double minLongitude = 0; //min y
                            GeoBoundingBox gbb = new GeoBoundingBox();
                            for (MapRoute tempMapRoute : mapRouteList) {
                                double top = tempMapRoute.getRoute().getBoundingBox()
                                        .getTopLeft().getLongitude();
                                if (top > maxLongitude) {
                                    maxLongitude = top;
                                }

                                double bottom = tempMapRoute.getRoute().getBoundingBox()
                                        .getBottomRight().getLongitude();
                                if (bottom > minLongitude) {
                                    minLongitude = bottom;
                                }

                                double left = tempMapRoute.getRoute().getBoundingBox()
                                        .getTopLeft().getLatitude();
                                if (left > minLatitude) {
                                    minLatitude = left;
                                }

                                double right = tempMapRoute.getRoute().getBoundingBox()
                                        .getBottomRight().getLatitude();
                                if (right > maxLatitude) {
                                    maxLatitude = right;
                                }
                            }
                            gbb.setBottomRight(new GeoCoordinate(maxLatitude, minLongitude));
                            gbb.setTopLeft(new GeoCoordinate(minLatitude, maxLongitude));
                            map.zoomTo(gbb, Map.Animation.BOW,
                                    Map.MOVE_PRESERVE_ORIENTATION);
                        }

                        mapRouteList.add(mapRoute);
                        map.addMapObject(mapRoute);


//                        Get the bounding box containing the route and zoom in


                    } else {
                        Toast.makeText(getApplicationContext(), String.format("Route calculation " +
                                        "failed: %s",
                                errorCode.toString()), Toast.LENGTH_SHORT).show();
                    }
                }

                public void onProgress(int percentage) {

                }
            };


    MapGesture.OnGestureListener listener =
            new MapGesture.OnGestureListener.OnGestureListenerAdapter() {
                @Override
                public boolean onMapObjectsSelected(List<ViewObject> objects) {
                    for (ViewObject viewObj : objects) {
                        if (viewObj.getBaseType() == ViewObject.Type.USER_OBJECT) {
                            if (((MapObject) viewObj).getType() == MapObject.Type.ROUTE) {

                            }
                            if (((MapObject) viewObj).getType() == MapObject.Type.MARKER) {
                                MapMarker mapMarker = ((MapMarker) viewObj);
                                String description = mapMarker.getDescription();
                                String title = mapMarker.getTitle();
                                if (title.equals("startEndPoints")) {
                                    if (startPoint == null) {
                                        startPoint = new GeoCoordinate(mapMarker.getCoordinate());
                                        Toast.makeText(getApplicationContext(),
                                                "Please select your destination",
                                                Toast.LENGTH_LONG).show();

                                    } else {
                                        endPoint = new GeoCoordinate(mapMarker.getCoordinate());
                                        if (startPoint != null && endPoint != null) {
                                            Toast.makeText(getApplicationContext(),
                                                    "Both points loaded ready to call api",
                                                    Toast.LENGTH_LONG).show();
                                        }
                                        map.removeMapObjects(startEndPointsMapObjects);
                                        startEndPointsMapObjects.clear();

                                        String startLat = Double.toString(startPoint.getLatitude());
                                        String startLong = Double.toString(startPoint
                                                .getLongitude());
                                        String endLat = Double.toString(endPoint.getLatitude());
                                        String endLong = Double.toString(endPoint.getLongitude());


                                        //Do api journey api call here
                                    }

                                } else {
                                    Toast.makeText(getApplicationContext(),
                                            title + " " + description,
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                    // return false to allow the map to handle this callback also
                    return false;
                }
            };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EventBus.getDefault().register(this);
        mRestApiRequest = new RestApiRequest();
        mDaoActiveAndroid = new DaoActiveAndroid();
        mParseActiveAndroid = new ParseActiveAndroid();
        //RequestPoints
        mRestApiRequest.requestGetPoints();

        mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id
                .mapfragment);
        mapFragment.init(new OnEngineInitListener() {
            @Override
            public void onEngineInitializationCompleted(Error error) {
                if (error == Error.NONE) {
                    onMapFragmentInitializationCompleted();
                } else {
                    Toast.makeText(getApplicationContext(), "Failed to initialize map engine",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        footerRadioGroup = (RadioGroup) findViewById(R.id.footer_radio_group);

        searchImageButton = (ImageButton) findViewById(R.id.search_imagebutton);
        searchImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //do api request here
                String selectedModeString = ((RadioButton) findViewById(footerRadioGroup
                        .getCheckedRadioButtonId())).getText().toString();


//                // 2. Initialize RouteManager
//                RouteManager routeManager = RouteManager.getInstance();
//                routeManager.calculateRoute(routePlanList.get(0), routeManagerListener);
            }
        });

        mapRouteList = new ArrayList<MapRoute>();
        routePlanList = new ArrayList<RoutePlan>();
        routeSpielsList = new ArrayList<String>();
        startEndPointsMapObjects = new ArrayList<MapObject>();
        routeSpielsList.add("First");
        routeSpielsList.add("Second");
        routeSpielsList.add("Third");
        routeSpielsList.add("Fourth");

        colorList = getResources().getIntArray(R.array.route_colors);

        //Do get all points api call here
    }


    private void onMapFragmentInitializationCompleted() {
        map = mapFragment.getMap();
        map.setCenter(new GeoCoordinate(14.5995124, 120.9842195, 0.0),
                Map.Animation.NONE);
        map.setZoomLevel((map.getMaxZoomLevel() + map.getMinZoomLevel()) / 2);
        mapFragment.getMapGesture().addOnGestureListener(listener);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void addStartEndPointMapMarkerToMap(List<PointsModel> pointsModelList) {
        Image image = new Image();
        image.setBitmap(BitmapFactory.decodeResource(getResources(),
                R.drawable.map_marker));

        for (PointsModel pointsModel : pointsModelList) {

            GeoCoordinate geoCoordinate = new GeoCoordinate(Double.parseDouble(pointsModel
                    .mStartLat), Double.parseDouble(pointsModel.mStartLong));
            MapMarker mapMarker = new MapMarker(geoCoordinate, image);
            mapMarker.setTitle("startEnd");
            mapMarker.setDescription(pointsModel.mStartLat + "," + pointsModel.mStartLong);

            GeoCoordinate geoCoordinate2 = new GeoCoordinate(Double.parseDouble(pointsModel
                    .mEndLat), Double.parseDouble(pointsModel.mEndLong));
            MapMarker mapMarker2 = new MapMarker(geoCoordinate2, image);
            mapMarker2.setTitle("startEnd");
            mapMarker2.setDescription(pointsModel.mEndLat + "," + pointsModel.mEndLong);

            map.addMapObject(mapMarker);
            map.addMapObject(mapMarker2);

            startEndPointsMapObjects.add(mapMarker);
            startEndPointsMapObjects.add(mapMarker2);


        }
    }

    private void plotJourneyToMap(JourneyModel journeyModel) {


        routesModelList = journeyModel.routes;


        RouteOptions routeOptions = new RouteOptions();
        routeOptions.setTransportMode(RouteOptions.TransportMode.PUBLIC_TRANSPORT);
        routeOptions.setRouteType(RouteOptions.Type.FASTEST);
        // 3. Select routing options via RoutingMode


        for (RoutesModel routesModel : routesModelList) {
            RoutePlan routePlan = new RoutePlan();
            routePlan.setRouteOptions(routeOptions);

            routePlan.addWaypoint(new GeoCoordinate(Double.parseDouble(routesModel.mStartLat),
                    Double.parseDouble(routesModel.mStartLng)));
            routePlan.addWaypoint(new GeoCoordinate(Double.parseDouble(routesModel.mEndLat),
                    Double.parseDouble(routesModel.mEndLng)));

            routePlanList.add(routePlan);
        }

        RouteManager routeManager = RouteManager.getInstance();
        routeManager.calculateRoute(routePlanList.get(0), routeManagerListener);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onEvent(SuccessRequestPoints successRequestPoints) {

    }

    public void onEvent(SuccessRequestJourney successRequestJourney) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
