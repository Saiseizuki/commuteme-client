package commuteme.here.com.commuteme.model;

import com.activeandroid.annotation.Column;
import com.google.gson.annotations.SerializedName;

/**
 * Created by jadeantolingaa on 12/6/14.
 */
public class ErrorResponseModel {

  @SerializedName("error")
  public String mError;

  public ErrorResponseModel(String errorMessage) {
    this.mError = errorMessage;
  }
}
