package commuteme.here.com.commuteme.event;

import commuteme.here.com.commuteme.model.ErrorResponseModel;

/**
 * Created by jadeantolingaa on 12/6/14.
 */
public class FailedRequestPoints {
  public ErrorResponseModel mErrorResponseModel;

  public FailedRequestPoints(ErrorResponseModel errorResponseModel) {
    this.mErrorResponseModel = errorResponseModel;
  }
}
